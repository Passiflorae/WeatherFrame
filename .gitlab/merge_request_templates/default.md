_Add description here_

_Description can contain multiple paragraph separated by an empty line._

_Be careful, the title and description of the merge request will be used to generate the new commit on master !!!_

BREAKING CHANGE: Keep this line if the merge request brings a breaking change and replace everything after `:` by the description of the breaking change
