SHELL=/bin/bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
default: help

CMD_MAIN = ""

OUTPUT_DIR="./build/${ARCH}/"
OUTPUT_BIN = "./${OUTPUT_DIR}/WeatherFrame"

ARCHIVE_NAME=
ifeq ($(ARCHIVE_NAME),)
ARCHIVE_NAME=WeatherFrame-$(shell git show-ref -s  --abbrev HEAD).tar.gz
endif


dev/setup: ## Setup dev environment
	# todo

dev/build: ## Live reload
	# todo


.PHONY: build
build: ## Build the project
	# todo

run: ## Run the project
	# todo

lint: ## Run the linter(golangci-lint)
	@golangci-lint run --color always

release: ## Create archive release
	printf '' | tar cvzf ${ARCHIVE_NAME} --files-from -


tests: ## Run tests
	# todo

help: ## Display the help message and quit
	@grep -E '^[[:print:]]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
